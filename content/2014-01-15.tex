% Vorlesung am 15.01.2014
% TeX: Michi

\renewcommand{\printfile}{2014-01-15}

\subsection{Zwei-Niveau-Atome}

\minisec{Lorentz Modell}
Das halbklassische \acct*{Modell nach Lorentz} \index{Lorentz Modell} beschreibt ein an ein Atom gebundenes Elektron, welches durch ein elektrisches Feld zur Oszillation angeregt wird. Die mathematische Modellierung entspricht der Differentialgleichung eines gedämpften harmonischen Oszillator, d.h. Atom und Elektron sind mit einer \frqq Feder\flqq\ gekoppelt (aus klassischer Sicht).
Während im klassischen Fall kontinuierliche Werte erreicht werden, sind sie quantenmechanisch diskret (Vgl. Abbildung~\ref{fig:2014-01-15_1}).

\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}
      \node[draw,circle,minimum size=1cm] (plus) at (0,0) {$+$};
      \node[draw,circle,minimum size=0.5cm] (minus) at (3,0) {$-$};
      \draw[decorate,decoration=snake] (plus) -- node[above] {$\omega_0$} (minus);
    \end{scope}
    \begin{scope}[shift={(4,-0.5)}]
      \draw (0,0) -- coordinate (g) (1,0) node[right] {$\ket{g}$};
      \draw (0,1) -- coordinate (e) (1,1) node[right] {$\ket{e}$};
      \draw[<->] (e) -- node[right] {$\omega_0$} (g);
      \draw[<-] (e) -- node[right] {$\omega_0$} ++(0,0.5);
    \end{scope}
  \end{tikzpicture}
  \caption{Darstellung des Lorentz Modell}
  \label{fig:2014-01-15_1}
\end{figure} 

\begin{notice}
  Wesentlicher Unterschied der der zum klassischen Lorentz Modell gemacht werden muss:
  \begin{itemize}
    \item Im klassischen kann beliebig viel Energie, bzw. Intensität im Oszillator gesteckt werden, d.h. die Amplitude wird beliebig groß 
    \item Hier gibt es allerdings eine Grenze der Amplitude (angeregtes Niveau) von dort geht es dann auch wieder zurück
  \end{itemize}
\end{notice}

Das klassische Lichtfeld wird hierbei durch
%
\begin{align*}
  \bm{E} &= \bm{E}_0 \cos\left(\omega_L t\right)
\end{align*}
%
beschrieben. Der Hamiltonoperator der Wechselwirkung ist also
%
\begin{align*}
  \hat{H}_{\mathrm{WW}} &= - \hat{\bm{d}} \cdot \bm{E}.
\end{align*}
% 
Es gilt $\hat{\bm{d}} = e \hat{\bm{r}}$ und $\hat{\bm{r}}$ ist der Ort des Elektrons gemessen vom Kernort. Die zeitabhängige Schrödingergleichung ist somit:
%
\begin{align}
  \ii \hbar \partial_t \psi &= \left(\hat{H}_0 + \hat{H}_{\mathrm{WW}}\right) \psi.
\end{align}
%
Die Eigenzustände von $\hat{H}_0$ sind hierbei \[\hat{H}_0 \psi_{1,2} = E_{1,2} \psi_{1,2}. \] Die Wellenfunktion $\psi$ stellt dabei eine Superposition der beiden Eigenfunktionen $\psi_1$ und $\psi_2$ dar
%
\begin{align}
  \psi &= c_1(t) \psi_1 \ee^{-\ii E_1 t / \hbar } + c_2(t) \psi_2 \ee^{-\ii E_2 t / \hbar }. \label{eq: 2014-01-15_1337}
\end{align}
%
$\psi_1$ und $\psi_2$ müssen Dipolübergangerlaubte Zustände sein, z.B.: beim Wasserstoffatom $slp$-Zustände. Mit den Anfangsbedingungen 
%
\begin{align*}
  c_1(t=0) &= 1 \\
  c_2(t=0) &= 0
\end{align*}
% 
und dem Dipolmoment
%
\begin{align*}
  \Braket{d(t)}_{\ket{\psi(t)}} &= \int \psi^\ast(t) \hat{\bm{d}} \psi(t) \diff V \\
  &= \bar{e} c_2^\ast c_1 \braket{1|\bm{r}|2} \ee^{\ii\omega_0 t} + \bar{e} c_1^\ast c_2 \braket{2|\bm{r}|1} \ee^{-\ii\omega_0 t} ,
\end{align*}
%
welches aus den Diagonaleinträgen besteht, folgt
%
\begin{align*}
  |c_2|^2 &= \frac{\omega_R^2}{\Omega^2} \sin^2\left(\frac{\Omega t}{2}\right).
\end{align*}
%
Für $\omega_R$ gilt hierbei
%
\begin{align}
  \boxed{ \omega_R = \Braket{1|e \bm{r} \cdot \bm{E}_0|2} = \frac{e}{\hbar} \bm{E}_0 \int \psi_1^\ast \bm{r} \psi_2 \diff V }
\end{align}
%
Dies entspricht der Rabifrequenz, wenn die Verstimmung $\Delta = 0$ ist. Allgemein jedoch ($\Delta \neq 0$) gilt (Vgl. Abbildung~\ref{fig:2014-01-15_3})
%
\begin{align}
  \boxed{\Omega = \sqrt{ \omega_R^2 + \Delta^2 } } 
\end{align}
%
\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[gfx]
    \draw (0,0) -- coordinate (g) (1,0);
    \draw (0,2) -- (1,2) ++(0.2,0) coordinate (e);
    \draw[dashed] (0,1.5) -- coordinate (delta) (1,1.5) ++(0.2,0) coordinate (delta1);
    \draw[->] (g) -- node[right] {$\omega_0$} (delta);
    \draw[decorate,decoration=brace] (e) -- node[right] {$\Delta$} (delta1);
  \end{tikzpicture}
  \caption{Darstellung der Verstimmung $\Delta \neq 0$}
  \label{fig:2014-01-15_3}
\end{figure}
%
Dies ist die verallgemeinerte \acct*{Rabifrequenz}.

\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (-0.3,0) -- (4.5,0) node[below] {$\Omega t$};
    \draw[->] (0,-0.3) -- (0,2.5) node[left] {$P_2 = |c_2|^2$};
    \draw (2,0.1) -- (2,-0.1) node[below] {$2 \pi$};
    \draw (4,0.1) -- (4,-0.1) node[below] {$4 \pi$};
    \draw (-0.1,2) node[left] {$1$} -- (0.1,2);
    \draw[DarkGreen,dotted] (-0.1,1) node[left] {$\frac{\omega_R^2}{\Omega^2}$} -- (4,1);
    \draw[Purple] plot[smooth,samples=61,domain=0:4.2] (\x,{2*sin(0.5*pi*\x r)^2});
    \draw[DarkGreen] plot[smooth,samples=61,domain=0:4.2] (\x,{sin(1.25*pi*\x r)^2});
  \end{tikzpicture}
  \caption{Rabifrequenz für verschiedenen $\Delta$}
  \label{fig:2014-01-15_4}
\end{figure}

Abbildung~\ref{fig:2014-01-15_4} zeigt:
%
\begin{itemize}
  \item Hier sieht man schön den Sättigungseffekt. Er oszilliert, steigt also nicht beliebig an für wachsende $\Omega t$ (im klassischen Fall würde ein beliebiges Anwachsen beobachtet werden). Die Charakteristik zeigt, dass nur 2 Niveaus existieren. 
  \item Der Anfängliche \frqq response\flqq\ für lila und grün ist genau gleich. Man kann wie folgt argumentieren: >>Bei kurzen Pulsen kann noch nicht entschieden werden ob $\Omega$ resonant (also ohne Verstimmung) oder nicht ist.\flqq
  \item Das zwei-Niveau-System entspricht somit einem Pseudospin.
  \item Wenn $c_1$ noch mit dazu kommt, bekommt man eine Phase mit (Minusvorzeichen).  Erst nach $4 \pi$ ist man wieder beim Ausgangszustand (vgl. Spin).
    \begin{itemize}
      \item $2 \pi$ Puls: $\ket{1} \to - \ket{1}$
      \item $4 \pi$ Puls: $\ket{1} \to  \ket{1}$
      \item $\pi$ Puls: $c_1 \ket{1} + c_2 \ket{2} \to - \ii \left[ c_1 \ket{2} + c_2 \ket{1} \right] \hateq - \ii \, \text{SWAP} $
      \item $\pi/2$ Puls:  $\ket{1} \to \frac{1}{\sqrt{2}} \left(\ket{1} - \ii \ket{2} \right)$, d.h. die Population im Grund- und angeregten Zustand ist gleich. 
    \end{itemize}
\end{itemize}
%
\begin{notice}
  Es zeigt sich, dass es zwischen einem zwei-Niveau-Atom und Spin $1/2$ einen Isomorphismus gibt.
\end{notice}


% 

\minisec{Dichtematrix}
Im folgenden wollen wir die \acct{Dichtematrix} einführen. 
%
\begin{align*}
  \varrho &\coloneq  \ket{\psi} \bra{\psi} \\
  &= \begin{pmatrix} c_1 \\ c_2  \end{pmatrix} \begin{pmatrix} c_1 & c_2  \end{pmatrix} \\
  &=\begin{pmatrix} |c_1|^2 & c_1 c_2^\ast \\ c_2 c_1^\ast & |c_2|^2 \end{pmatrix} \\
  &=\begin{pmatrix} \varrho_{11} & \varrho_{12} \\ \varrho_{21} & \varrho_{22} \end{pmatrix}
\end{align*}
% 
Wie jede $2\times 2$-Matrix lässt sich auch die Dichtematrix mit Hilfe der Pauli-Matrizen und der Einheitsmatrix ausdrücken. 
%
\begin{align}
  \varrho = \frac{1}{2} 
  \begin{pmatrix}
    1 + R_z & R_x - \ii R_y \\
    R_x + \ii R_y & 1 - R_z 
  \end{pmatrix} = \frac{1}{2} \left( \mathds{1}  + \bm{R} \cdot \bm{\sigma} \right)
  \label{eq: 2014-01-15-korrektur-1}
\end{align}
%
Dabei ist $\bm{R}$ der sog. Blochvektor. In unserem Fall ergibt sich dieser zu
%
\begin{align}
  \bm{R} =
  \begin{pmatrix}
  u \\
  v \\
  - w
  \end{pmatrix} = 
  \begin{pmatrix}
  \varrho_{12} + \varrho_{21} \\
  - \ii (\varrho_{12} - \varrho_{21}) \\
   \varrho_{11} - \varrho_{22}
  \end{pmatrix} .
  \label{eq: 2014-01-15-korrektur-2}
\end{align}
%
Für den reduizierten Hamiltonoperator ($H_{red}=H_{Atom} + H_{WW}$) -- der daraus folgt, dass über $n-1$ Moden ausgespurt wurde, sodass nur ein einzige übrig bleibt -- ergibt sich in Matrixdarstellung
%
\begin{align}
  H = \hbar 
  \begin{pmatrix}
    \omega_0 & \omega_R \\
    \omega_R^* & 0 \\
  \end{pmatrix} .
 \label{eq: 2014-01-15-korrektur-3}
\end{align}
%
Aus der Von-Neumann-Gleichung 
%
\begin{align}
  \dot{\varrho} = - \frac{\ii}{\hbar} \left[ H, \varrho \right]
\end{align}
%
lassen sich die \acct{Blochgleichungen} ableiten. Diese lauten
%
\begin{align}
  \dot{u} &= \delta v \\
  \dot{v} &= -\delta u + \omega_R w \\
  \dot{w} &= - \omega_R v .
  \label{eq: 2014-01-15-korrektur-4}
\end{align}
%
%
$u$ ist hierbei die Komponente in Phase, $v$ die Komponente außer Phase ($\pi /2$)  mit treibenden Feld. Nur $u$ liefert einen Beitrag zur Lichtkraft, siehe Abbildung~\ref{fig:2014-01-15_5}.
%
\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (-0.5,0) -- (2,0) node[right] {$\bm{E}$};
    \draw[->,MidnightBlue] (0,0) -- node[below] {$u$} (1,0);
    \draw[->,MidnightBlue] (1,0) -- node[right] {$v$} (1,1);
    \draw[->] (0,0) -- (1,1);
  \end{tikzpicture}
  \caption{Zur Veranschaulichung von $u$ und $v$ im Bezug zur Lichtkraft}
  \label{fig:2014-01-15_5}
\end{figure}
%
Der Blochvektor gibt die Position auf der \acct{Blochsphäre} an, siehe dazu Abbildung~\ref{fig:2014-01-15_6} 
%
\begin{figure}[htbp]
  \centering
  \tdplotsetmaincoords{80}{10}
  \begin{tikzpicture}[gfx,tdplot_main_coords]
    \coordinate (O) at (0,0,0);
    \tdplotsetthetaplanecoords{0}
    \tdplotdrawarc[draw=none,ball color=white,tdplot_rotated_coords]{(O)}{2}{0}{360}{}{}
    \tdplotdrawarc[dashed]{(O)}{2}{0}{180}{}{}
    \tdplotdrawarc{(O)}{2}{180}{360}{}{}
    \tdplotsetthetaplanecoords{0}
    \tdplotdrawarc[tdplot_rotated_coords]{(O)}{2}{0}{360}{}{}
    \tdplotsetthetaplanecoords{90}
    \tdplotdrawarc[MidnightBlue,dashed,tdplot_rotated_coords]{(O)}{2}{0}{180}{}{}
    \tdplotdrawarc[MidnightBlue,tdplot_rotated_coords]{(O)}{2}{180}{360}{}{}
    \node[black,dot,label={below:$\psi_1$}] (psi1) at (0,0,-2) {};
    \node[black,dot,label={above:$\psi_2$}] (psi2) at (0,0,2) {};
    \draw[->,DarkOrange3] (O) -- (3,0,0) node[right] {$\Omega$};
    \draw[->,DarkRed] (O) -- node[right=0.2cm] {$\bm{R}$} (psi1);
  \end{tikzpicture}
  \caption{Blochsphäre}
  \label{fig:2014-01-15_6}
\end{figure}
%
Alle Überlagerungszustände von $\psi_1$ und $\psi_2$ mit maximalen Dipolmoment liegen auf der Äquatorebene der Blochsphäre. Des weiteren lassen sich die Blochgleichungen als klassische Kreiselgleichung umschreiben:
%
%
\begin{align*}
  \bm{\Omega} &= \begin{pmatrix} \omega_R \\ 0 \\ \delta \end{pmatrix} \\
  \dot{\bm{R}} &= \bm{R} \times \bm{\Omega}
\end{align*}
%
$\dot{\bm{R}}$ entspricht einer Kreiselgleichung (Berry Phase unterscheidet dies vom klassischen Kreisel). Es zeigt sich also, dass die Blochgleichungen analog zur Kreiselgleichung der Bewegung sind.
