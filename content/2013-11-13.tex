% Vorlesung am 13.11.2013
% TeX: Michi

\renewcommand{\printfile}{2013-11-13}

\section{Hyperfeinstruktur und Zeemaneffekt im Wasserstoffatom}

\minisec{Hyperfeinstruktur}
Der Atomkern hat im allgemeinen elektrische- und magnetische Multipole. Die Wechselwirkung von Kernspin und Elektronspin führt zur \acct{Hyperfeinstruktur}, sie ist wesentlich kleiner als die Feinstruktur weshalb zu deren Messung im allgemeinen eine besonders hohe spektrale Auflösung erforderlich ist.  

\begin{notice*}[Fragen]
\begin{enumerate}
  \item Wie kann man die Hyperfeinstrukturaufspaltung berechnen? 
  \item Wie groß ist $\bm{\mu}_K \cdot \bm{B}_e(r=0)$ ?
\end{enumerate}
\end{notice*}

Im Folgenden wird der s-Zustand des H-Atoms betrachtet, wobei der Atomkern ein magnetisches Moment $\bm{\mu}_K$ besitzt, bzw. einen Kernspin und das Elektron einen Elektronenspin. Vgl. Abbildung~\ref{fig:2013-11-13-1}.
%
\begin{figure}[htpb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw (0,0) circle (1.2);
    \node at (0,0) {$+$};
    \draw[DarkOrange3,->] (.1,-0.3) -- (.1,0.3) node[right] {$\mu_k$};
    \begin{scope}[shift={(.8,-0.3)}]
      \node[MidnightBlue,dot] at (0,0) {};
      \draw[MidnightBlue,->] (0,-0.3) -- (0,0.3) node[right] {$e^-$};
    \end{scope}
  \end{tikzpicture}
  \caption{Atomkern mit magnetischen Moment und Elektron mit Spin im Wasserstoffatom bezüglich eine $s$-Zustands}
  \label{fig:2013-11-13-1}
\end{figure}
%
Für die Magnetisierung gilt
%
\begin{align}
  \bm{M}(\bm{r}) &= - g_e \mu_B \bm{S}\cdot\left|\psi_{100}(r)\right|^2 \label{eq: 2013-13-11--00}
\end{align}
%
wobei der Term $\bm{S}\cdot\left|\psi_{100}(r)\right|^2$ die \acct{Spindichte} repräsentiert und $g_e \mu_B \bm{s}\cdot\left|\psi_{100}(r)\right|^2$ die \acct*{magnetische Dipoldichte} \index{Dipoldichte !magnetische}. Zerlegen wir unser Problem in Kugeln mit konstanter Magnetisierung $M_i$, so gilt für die Gesamtmagnetisierung am Koordinatenursprung
%
% \begin{figure}[htpb]
%   \centering
%   \begin{tikzpicture}[gfx]
%     \node[dot] at (0,0) {};
%     \foreach \i in {0.25,0.5,0.75} {
%       \draw[DarkOrange3] (0,0) circle (\i);
%     }
%   \end{tikzpicture}
%   \caption{Berechnung der Magnetisierung am Kernort durch Aufteilen in konzentrischen Kugelschalen}
%   \label{fig:2013-11-13-2}
% \end{figure}
%
\begin{align*}
  M(0) &= \sum\limits_{i} M_i
\end{align*}
%
\begin{notice*}
  Aus der Elektrodynamik wissen wir, das eine Kugel mit konstanter Magnetisierung ein Magnetfeld am Ursprung erzeugt, für das 
  \begin{align}
    \bm{B}(0) = \frac{2}{3} \mu_0 \bm{M} \label{eq: 2013-11-13--01}
  \end{align}
  gilt (siehe \fullcite{jackson:1962}).
\end{notice*}

Unter Verwendung von Gleichung~\eqref{eq: 2013-11-13--01} gilt für das Magnetfeld am Kernort
%
\begin{align*}
  \bm{B}(0) &= \frac{2}{3} \mu_0 \sum\limits_{i} M_i \\
  &= \frac{2}{3} \mu_0 \bm{M}_e(0)s
\end{align*}
%
und mit Gleichung~\eqref{eq: 2013-13-11--00} 
%
\begin{align}
  \bm{B}_e(0) &= -\frac{4}{3} \mu_0 \mu_B \bm{S}\cdot \underbrace{\left|\psi_{100}(r)\right|^2}_{=\frac{1}{\pi a_0^3}} \\
  &= -\frac{4 \mu_0 \mu_B}{3 \pi a_0^3} \bm{S}
\end{align}
%
Für den Wechselwirkungsterm im Hamiltonoperator folgt somit
%
\begin{align}
  H_{HF} &= - \bm{\mu}_p \bm{B}_e  \nonumber \\
  &= \frac{4 \mu_0}{3 \pi a_0^3} g_p \mu_K \mu_B \bm{I}\cdot \bm{S} \label{eq: 2013-13-11--02}
\end{align}
%
Die in der Gleichung aufkommenden Konstanten sind in Tabelle~\ref{tab: 2013-13-11--01} mit ihren Definitionen aufgeführt.
%
\begin{table}[htpb]
  \centering
  \begin{tabular}{ccc}
    \toprule
    Konstante  & Kennzeichnung & Definition \\
    \midrule
    Bohrmagneton & $\mu_B$ & $\dfrac{e \hbar}{2 m_e c}$ \phantom{$\Bigg|$} \\
    Kernmagneton & $\mu_K$ & $\dfrac{e \hbar}{2 m_p c}$ \phantom{$\Bigg|$} \\
    \bottomrule
  \end{tabular}
  \caption{Definitionen von Kern- und Bohrmagneton}
  \label{tab: 2013-13-11--01}
\end{table}
%
Im Fall des Wasserstoffatoms nimmt der $g$-Faktor $g_p$ den Wert $5.58$ an. Für Gleichung~\eqref{eq: 2013-13-11--02} bedeutet dies
%
\begin{align}
  H_{HF} &= \frac{a}{\hbar^2} \bm{I} \cdot \bm{S} \label{eq: 2013-13-11--03}
\end{align}
%
Mit $F = I + S$ können wir folgende Relation herleiten
%
\begin{align}
  F^2 -I^2 -S^2 &= \left(I+S\right)^2 - I^2 -S^2 \nonumber \\
  &= 2 I \cdot S \label{eq: 2013-13-11--04}
\end{align}
% 
Gleichung~\eqref{eq: 2013-13-11--04} in Gleichung~\eqref{eq: 2013-13-11--03}:
%
\begin{align}
  H_HF &= \frac{a}{2 \hbar^2} \left(F^2-I^2-S^2\right) \label{eq: 2013-13-11--05}
\end{align}
%
Für die Konstante $a$ gilt:
\[
  a = 5.58 \cdot \frac{4 \mu_0 \hbar^2}{4 \pi a_0^3} \mu_K \mu_B \approx \SI{1420}{\mega\Hz} \cdot h.
\]
Ausgedrückt durch die Quantenzahlen gilt für Gleichung~\eqref{eq: 2013-13-11--05} unter Berücksichtigung, dass sowohl Elektronenspin als auch Kernspin den Wert $1/2$ haben:
%
\begin{align*}
  H_F &= \frac{a}{2}\left[F(F+1) - I(I+1) - S(S+1)\right] \\
  &= \frac{a}{2}\left[F(F+1) - \frac{3}{2}\right] \\
  &= \begin{dcases}  \frac{a}{4} , & F=1 \\ -\frac{3a}{4} , & F=0\end{dcases}
\end{align*}
%

\begin{notice*}
  Durch die Kopplung $\bm{I}\cdot\bm{S}$ werden die Kern- und Elektronenzustände verschränkt, d.h. der unkorrelierte Produktzustand
  \[
    \Ket{I,m_I}\Ket{S,m_S} = \Ket{I,m_I,S,m_S}
  \]
  ist keine Eigenbasis mehr unter der $\bm{I} \cdot \bm{S}$ Kopplung (jedoch weiterhin eine Orthonormalbasis).Die neue Eigenbasis ist jetzt
  \[
    \Ket{F,m_F,I,S} = \sum\limits_{m_I,m_S} \Ket{I,m_I,S,m_S}\underbrace{\Braket{I,m_I,S,m_S|F,m_F,I,S}}_{(\ast)}.
  \]
  $(\ast)$ sind die sogenannten \acct{Clebsch-Gordon-Koeffizienten}. Es handelt sich dabei um Entwicklungskoeffizienten, mit denen man aus der Basis der Einzeldrehimpulse, bzw. Einzelspins in die Basis des Gesamtdrehimpulses, bzw. Gesamtspins übergeht. Die einzelnen Koeffizienten könne nach allen Regeln der Drehimpulsgymnastik bestimmt werden.
\end{notice*}

\minisec{Zeemaneffekt}
Durch anlegen eines schwachen Magnetfeldes erwarten wir eine Aufspaltung der Energieterme wie in Abbildung~\ref{fig:2013-11-13-3}, den \acct{Zeemaneffekt}. Schwach heißt hierbei, dass die Feinstrukturaufspaltung groß gegenüber dem Magnetfeld ist, d.h.
%
\begin{align}
  \Delta E_{HFS} > \mu_B B .
  \label{eq: 2013-13-11-Korrektur1}
\end{align}
 Die für $F=1$ skizzierte Aufspaltung entspricht hierbei dem linearen Zeemaneffekt (aus der entarteten Störungstheorie) und die für $F=0$ entspricht einem quadratischen Zeemaneffekt (aus der nicht entarteten Störungstheorie). Der Wechselwirkungsterm im Hamiltonian entspricht:
%
\begin{figure}[htpb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}[shift={(0,2)}]
      \draw (0,0) node[left] {$F = 1$} -- (1,0);
      \foreach \i in {-0.5,0,0.5} {
        \draw[dashed] (1,0) -- +(1,\i);
      }
      \node[text width=0.5\textwidth,right] at (3,0) {linearer Zeemaneffekt wegen entarteter Störungstheorie};
    \end{scope}
    \begin{scope}
      \draw (0,0) node[left] {$F = 0$} -- (1,0);
      \draw[dashed] (1,0) parabola (2.5,-0.5);
      \node[text width=0.5\textwidth,right] at (3,0) {quadratischer Zeemaneffekt wegen \glqq nicht entarteter Störungstheorie\grqq};
    \end{scope}
  \end{tikzpicture}
  \caption{Lineare und quadratische Zeemanaufspaltung am Wasserstoffatom}
  \label{fig:2013-11-13-3}
\end{figure}
%
\begin{align}
  \hat{H}_B &= - \hat{\bm{\mu}}_e \cdot \bm{B} = 2 \mu_B \bm{B} \cdot \hat{\bm{S}}_z. \label{eq: 2013-13-11--06}
\end{align}
%  
Für die Basiszustände gilt:
%
\begin{align}
  \Ket{F=1,m_F = 1} &= \Ket{+}_S\Ket{+}_I \label{eq: 2013-13-11--06a} \\
  \Ket{F=1,m_F = 0} &= \frac{1}{\sqrt{2}}\left(\Ket{+}_S\Ket{-}_I + \Ket{-}_S\Ket{+}_I \right) \label{eq: 2013-13-11--06b}\\
  \Ket{F=1,m_F = -1} &= \Ket{-}_S\Ket{-}_I \label{eq: 2013-13-11--06c} \\
  \Ket{F=0,m_F = 0} &= \frac{1}{\sqrt{2}}\left(\Ket{+}_S\Ket{-}_I - \Ket{-}_S\Ket{+}_I \right) \label{eq: 2013-13-11--06d}
\end{align}
%
\begin{notice*}
  Der Zustand ~\eqref{eq: 2013-13-11--06a} ausgedrückt in der alten Basis ist:
  \[
  \Ket{F=1,m_F = 1} \hateq \Ket{S=\frac{1}{2},m_S = +\frac{1}{2},I=\frac{1}{2},m_I = \frac{1}{2}}.  
  \]
\end{notice*}
%
Gleichung~\eqref{eq: 2013-13-11--06a} und Gleichung~\eqref{eq: 2013-13-11--06c} werden auch als gestreckte Zustände bezeichnet. Gleichung~\eqref{eq: 2013-13-11--06b} ist ein symmetrischer Zustand, Gleichung~\eqref{eq: 2013-13-11--06a} ein antisymmetrischer (analog für Rest).

Werden stärkere (vgl. \eqref{eq: 2013-13-11-Korrektur1}) Magnetfelder angelegt, betrachten wir
%
\begin{align}
  \hat{H}_{\mathrm{ges}} &= a \bm{I} \cdot \bm{S} + 2 \mu_B \bm{B} \cdot \hat{\bm{S}}_z
\end{align}
%
Die einzelnen Matrixelemente sind in Tabelle~\ref{tab: 2013-11-13-3} zu sehen. 
%
\begin{table}[htpb]
  \centering
  \begin{tabular}{ccccc}
    \toprule
    $F,m_F$ & $1,1$ & $1,-1$ & $1,0$ & $0,0$ \\
    \midrule
    $1,1$ & $\frac{a}{4}+ \mu_0 B -E$ & & & \\
    $1,-1$ & & $\frac{a}{4}- \mu_0 B -E$ & & \\
    $1,0$ & & & $\frac{a}{4} + E$ & $\mu_B B$  \\
    $0,0$ & & & $\mu_B B$ & $\frac{a}{4} + E$ \\
    \bottomrule
  \end{tabular}
  \caption{Matrixelemente des Hamiltonian}
  \label{tab: 2013-11-13-3}
\end{table}
%
Wir lösen also die Schrödingergleichung:
%
\begin{align*}
  \hat{H} \Ket{\psi} &= E \Ket{\psi} \\
  \implies \det\left(\hat{H} - E \mathds{1}\right) &\stackrel{!}{=} 0 
\end{align*}
%
Dies bedeutet 
\[
  \det \tikz[baseline=-0.5ex] {
    \matrix (A) [matrix of nodes,left delimiter=(,right delimiter=),ampersand replacement=\&] {
      $\frac{a}{4}+ \mu_0 B -E$ \& $0$ \& $0$ \& $0$ \\
      $0$ \& $\frac{a}{4}- \mu_0 B -E$ \& $0$ \& $0$ \\
      $0$ \& $0$ \& $\frac{a}{4} - E$ \& $\mu_B B$ \\
      $0$ \& $0$ \& $- \mu_B B$ \& $\frac{a}{4} - E$ \\
    };
    \draw[dotted] (A-1-2.north -| A-2-2.east) -- (A-4-2.south -| A-2-2.east);
    \draw[dotted] (A-2-2.south -| A-1-1.west) -- (A-2-2.south -| A-4-4.east);
  } = 0
\]
%\[
%  \det \begin{pmatrix}
%  \frac{a}{4}+ \mu_0 B -E & 0 & 0 & 0\\
%  0 & \frac{a}{4}- \mu_0 B -E & 0 & 0\\
%  0 & 0 & \frac{a}{4} + E & \mu_B B  \\
%  0 & 0 & \mu_B B &  \frac{a}{4} + E 
%  \end{pmatrix} = 0
%\]
Mit Hilfe der Blockstruktur können die Eigenwerte bestimmt werden, diese sind
%
\begin{align}
  E_{1/2} &= \frac{a}{4} \pm \mu_B B \label{eq: 2013-13-11--07a} \\
  E_{3/4} &= -\frac{a}{4} \pm \frac{a}{2} \sqrt{1+ \left(\frac{2 \mu_B B}{a}\right)^2} \label{eq: 2013-13-11-07b}
\end{align}
%
Gleichung~\eqref{eq: 2013-13-11--07a} sind hierbei die Eigenwerte zu den gestreckten Zuständen (linearer Zeemaneffekt), und Gleichung~\eqref{eq: 2013-13-11-07b} sind die Eigenwerte des quadratischen Zeemaneffekt, allerdings bei kleinen Feldern (für große Felder wieder linear). Abbildung~\ref{fig:2013-11-13-4} veranschaulicht die Aufspaltung.
%
\begin{figure}[htpb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (0,-2) -- (0,4);
    \draw (0.1,1) -- (-0.1,1) node[left] {$F=1$};
    \draw (0.1,0) -- (-0.1,0) node[left] {$F=0$};
    \draw[MidnightBlue] (0,1) -- node[above left] {$\ket{+}\ket{+}$} (4,4);
    \draw[MidnightBlue] (0,1) -- node[above right] {$\ket{-}\ket{-}$} (4,-2);
    \draw[DarkOrange3] (0,1) .. controls (1,1) .. node[pos=0.7,right,anchor=north west,text width=5cm] {hat $\ket{+}\ket{-}$ Charakter für $\mu_B \gg a$ d.\,h.\ diese beiden Zustände verändern ihre Eigenbasis mit $B$.} (4.75,4);
    \draw[DarkOrange3] (0,0) .. controls (0.75,0) .. node[pos=0.8,below left] {$\ket{-}\ket{+}$} (3.25,-2);
  \end{tikzpicture}
  \caption{Lineare und quadratische Zeemanaufspaltung im Wasserstoffatom}
  \label{fig:2013-11-13-4}
\end{figure}
%

\begin{notice}[Vorsicht]
  Wir haben vernachlässigt, dass $\bm{\mu} = \bm{\mu}_e + \bm{\mu}_K$ ist. Korrekt wäre:
  \[
    \hat{H}_B = - \bm{\mu} \cdot \bm{B} = g_e \mu_B B S_z - g_p \mu_K B I_z
  \]
  d.h. für $g_p \mu_K B I_z \approx a/2$ ist eine Kreuzung zu beobachten ($B = \SI{16.7}{\tesla}$).
\end{notice}
