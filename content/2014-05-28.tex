% Vorlesung am 28.05.2014
% TeX: Michi

\renewcommand{\printfile}{2014-05-28}

Wie wir also gesehen haben, kann die Streulänge als effektiver Radius für ein Harte-Kugelpotential angesehen werden. Dieses Potential bezeichnet man auch als Pseudopotential. 

\subsection{Streuung am Kastenpotential} \index{Kastenpotential}
%
Im Folgenden betrachten wir das Verhalten einer von links nach rechts laufenden Welle in ein Kastenpotential, wie es in Abbildung~\ref{fig:2014-05-28-1} zu sehen ist. 
%
\begin{figure}[ht]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (0,-1.2) -- (0,1.2) node[left] {$V$};
    \draw[->] (-.1,0) -- (4.2,0) node[below] {$r$};
    \draw[DarkOrange3] (0,-1) node[left] {$-V_0$} -| (1.5,0) node[below right] {$r_0$} -- (4,0);
    \draw plot[smooth,samples=71,domain=0:1.45] (\x,{.5*sin(6*pi*\x r)+.5}) -- plot[smooth,samples=71,domain=1.58:4] (\x,{.5*sin(2*pi*\x r)+.5});
    \draw[dashed] plot[smooth,samples=71,domain=0:4] (\x,{.5*sin(pi*\x r)+.5});
    \path (2.25,1.1) coordinate (a) -- node[above] {$\delta$} (2.5,1.1) coordinate (b);
    \draw[|<-] (a) -- +(-.3,0);
    \draw[|<-] (b) -- +(.3,0);
    \path[MidnightBlue] (.75,1.1) coordinate (a) -- node[above] {$k_0$} (1.083,1.1) coordinate (b);
    \draw[|<-,MidnightBlue] (a) -- +(-.3,0);
    \draw[|<-,MidnightBlue] (b) -- +(.3,0);
  \end{tikzpicture}
  \caption{Kastenpotential mit einfallender Welle von links sowie einfallende Welle ohne Potential (freie Lösung)}
  \label{fig:2014-05-28-1}
\end{figure}
%
Hierbei gelten die Anschlussbedingung und Stetigkeitsbedingung
%
\begin{align*}
  \psi_1(r<r_0) &\approx \sin \left(\sqrt{k^2 + q^2} r\right) \\
  \psi_2(r>r_0) &\approx \sin \left(k r + \delta \right),
\end{align*}
%
wobei 
%
\begin{align*}
  k^2 = \frac{2m}{\hbar^2} E_{\mathrm{th}} \quad,\quad q^2 = \frac{2m}{\hbar^2} V_0.
\end{align*}
%
Aus den Anschlussbedingungen 
%
\begin{align*}
  \psi_1(r_0) &= \psi_2(r_0) \\
  \psi_1'(r_0) &= \psi_2(r_0)'
\end{align*}
%
folgt für $\delta$
%
\begin{align}
  \delta &= - k r_0 + \arctan\left(\frac{k \tan\left(\sqrt{q^2 + k^2}r_0\right)}{\sqrt{q^2 + k^2}}\right).
\end{align}
%
Für die Streulänge $a$ gilt
%
\begin{align*}
  a &= - \lim\limits_{k \to 0} \frac{\tan \delta }{k} \\
  &= - \lim\limits_{k \to 0} - r_0 + \frac{\tan\left(\sqrt{q^2 +k^2 }r_0\right)}{\sqrt{q^2 +k^2}}.
\end{align*}
%
und somit
%
\begin{align}
  \boxed{ a = r_0 - \frac{\tan(qr_0)}{q} }
\end{align}
%
Tragen wir $a$ über $q r_0$ auf, so erhalten wir Abbildung~\ref{fig:2014-05-28-2}. Wir sehen, dass in der Nähe der Resonanzen die Streulängen extrem negativ oder positiv werden, bzw. sich nicht im Bereich der meisten Atome bei $r_0$ befinden.
%
\begin{figure}[ht]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (0,-2.2) -- (0,2.2) node[left] {$a$};
    \draw[->] (-.1,0) -- (6,0) node[below] {$q\cdot r_0$};
    \foreach \i in {1,2,...,5} {
      \pgfmathparse{int(mod(\i,2))}
      \ifnum\pgfmathresult=1
        \draw[dotted] (\i,2.2) -- (\i,-2.2);
      \fi
      \draw (\i,.1) -- (\i,-.1) node[below] {$\pgfmathparse{\i/2}\pgfmathresult\pi$};
    }
    \draw[MidnightBlue] plot[smooth,domain=0:1-.06] (\x,{-.2*tan(pi/2*\x r)}) node[below left,align=center,text width=2.5cm] {1. gebundener Zustand};
    \draw[name path=n1,MidnightBlue] plot[smooth,domain=1+.07:3-.05] (\x,{-.2*tan(pi/2*\x r)+.5}) node[below,align=center,text width=2.5cm] {2. gebundener Zustand};
    \draw[name path=n2,MidnightBlue] plot[smooth,domain=3+.07:5-.05] (\x,{-.2*tan(pi/2*\x r)+.5}) node[below right,align=center,text width=2.5cm] {3. gebundener Zustand};
    \draw[name path=n0,DarkOrange3] (0,.5) -- (6,.5) node[above] {$r_0$};
    \draw[DarkOrange3,name intersections={of=n0 and n1,name=i}] node[dot] at (i-1) {};
    \draw[DarkOrange3,name intersections={of=n0 and n2,name=i}] node[dot] at (i-1) {};
  \end{tikzpicture}
  \caption{Resonanzen der Streulänge in Abhängigkeit von $qr_0$}
  \label{fig:2014-05-28-2}
\end{figure}
%
\subsection{Temperaturabhängigkeit der $s$-Wellenstreuung}

Für den Wirkungsquerschnitt und die Streuamplitude von $s$-Wellen gilt
%
\begin{align*}
  \sigma_0(k) &= \frac{4 \pi }{k^2} \sin^2(\delta_0(k)) = |f_0|^2 \int\diff\Omega \\
  f_0(k) &= \frac{1}{k \cot(\delta(k)) - \ii k} \approx \frac{1}{-1/a - \ii k}.
\end{align*}
%
Somit ergibt sich für verschiedene Größen von $a$ 
%
\begin{align}
  \boxed{\sigma_0(k) =  \frac{4 \pi a^2}{1+k^2a^2}} \implies \begin{cases}
  \sigma_0 = \frac{4 \pi}{k^2} &\text{für $a \to \infty$} \\
  \sigma_0 = 4 \pi a^2 &\text{für $a \ll \lambda_{\mathrm{dB}}$} 
  \end{cases}.
\end{align}
%
\begin{notice}
  Für jede Temperatur existiert eine ausgezeichnete Partialwelle, d.h. die thermische Energie entspricht der eines gebundenen Zustandes. 
\end{notice}
%
Für die $s$-Welle ist die Temperaturabhängigkeit in Abbildung~\ref{fig:2014-05-28-3} zu sehen.
%
\begin{figure}[ht]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (-.1,0) -- (4.2,0) node[below] {$k$};
    \draw[->] (0,-.1) -- (0,2) node[left] {$\sigma$};
    \draw (1.5,.1) -- (1.5,-.1) node[below] {$2\pi/\lambda_\textnormal{dB}$};
    \draw (-.1,1.5) node[left] {$4\pi a^2$} -- (0,1.5) to[out=0,in=180] node[pos=0.2,above] {\color{DarkOrange3}$\sigma_0$} (4,0);
  \end{tikzpicture}
  \caption{Wirkungsquerschnitt über der Temperatur einer $s$-Welle}
  \label{fig:2014-05-28-3}
\end{figure}
%
Die Reichweite des Potenzials wird über die van der Waalslänge bezeichnet. D.h. $r_0$ entspricht im vdW-Fall
%
\begin{align}
  \frac{\hbar^2 }{2m} \left(\frac{2 \pi }{r_0}\right)^2 = \frac{C_6}{r_0^6} \implies r_0 = \left[ \frac{2 m }{\hbar^2} C_6 \right]^{\frac{1}{4}}.
\end{align}
%

\section{Feshbachresonanzen} \index{Feshbachresonanzen}

Im Folgenden betrachten wir zwei Molekülpotentiale, die nahezu übereinander liegen, wie sie in Abbildung~\ref{fig:2014-05-28-4} zu sehen sind. Die beiden Kanäle seien über eine anisotrope Kopplung $\Lambda$ miteinander verbunden. 
%
\begin{figure}[ht]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (0,-2.2) -- (0,2.2) node[left] {$V$};
    \draw[->] (-.1,0) -- (6,0) node[below] {$r$};
    \draw (.3,2) to[out=-85,in=210] (2,-1.5) to[out=30,in=180] coordinate[pos=.5] (a) (5,0) node[below=.3cm] {offener Kanal};
    \draw[MidnightBlue] (.5,2) to[out=-85,in=210] (2,-.5) to[out=30,in=180] coordinate[pos=.3] (b) (5,1) node[above] {geschlossener Kanal};
    \draw[DarkOrange3,<->] (a) -- node[auto] {$\Lambda$} (b);
    \filldraw (5,.2) circle (.2);
    \draw[->] (5-.3,.2) -- +(-.4,0);
    \begin{scope}[shift={(3.5/2+.7/2,.3)}]
      \draw (-1.5,0) -- (1.5,0);
      \draw[MidnightBlue,thick] plot[smooth,domain=-1.5:1.5] (\x,{-0.4*sin(5.5*pi*\x r)*sin(1.3*\x r)});
    \end{scope}
  \end{tikzpicture}
  \caption{Zwei Molekülpotentiale, die nahezu übereinander liegen. Das untere wird als offener Kanal, das obere als geschlossener Kanal bezeichnet. $\Lambda$ ist ein Kopplung. }
  \label{fig:2014-05-28-4}
\end{figure}
%
Mit anderen Worten führt jede anisotrope Wechselwirkung zu einer Kopplung $\Lambda$ zwischen den Potentialen. Als vereinfachte Modellvorstellung könne zwei übereinanderliegende Kastenpotentials mit unterschiedlicher Kastentiefe $V_0$ dienen, wobei die Höhe des einen Kastens mittels eine Magnetfeldes varriert werden kann. Siehe dazu Abbildung~\ref{fig:2014-05-28-5}.
%
\begin{figure}[ht]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[DarkOrange3] (0,2) -- (0,-2) -- (2,-2) -- (2,0) -- (4,0);
    \draw[dashed,MidnightBlue] (0,2) -- (0,-1) -- (2,-1) -- (2,1) -- (4,1);
    \draw[<->,MidnightBlue] (1,-1) -- node[auto] {$V_\textnormal{in}$} (1,-2);
    \draw[<->,MidnightBlue] (3,1) -- node[auto] {$V_\textnormal{out}$} (3,0);
    \draw[<->,DarkOrange3] (3,0) -- node[auto] {$V_0$} (3,-2);
    \draw[<->,Purple] (3.5,1-.2) -- (3.5,1+.2) node[above] {$\mu_B B$};
    \draw[<-,Purple] (4,.5) -- node[above] {$E_\textnormal{in}$} (5,.5);
  \end{tikzpicture}
  \caption{Modell zweier übereinanderliegenden Kastenpotentialen, mit einem Höhe variierenden Magnetfel.}
  \label{fig:2014-05-28-5}
\end{figure}
%
Der Hamiltonoperator $\hat{H}$ des Systems ist durch 
%
\begin{align}
  \hat{H} &= \underbrace{\begin{pmatrix}
    \frac{p^2}{2m}-V_0 & \Lambda \\
    \Lambda & \frac{p^2}{2m}+V_{\mathrm{In}-V_0 + \mu_\mathrm{B} B}
  \end{pmatrix}}_{r<r_0} +
  \begin{pmatrix}
    \frac{p^2}{2m} & 0 \\
    0 & \frac{p^2}{2m} + V_{\mathrm{Out}} + \mu_\mathrm{B} B
  \end{pmatrix} \label{eq:2014-05-28-1}
\end{align}
%
gegeben. Als Lösungsansatz der Schrödingergleichung setzen wir eine zwei komponentige Wellenfunktion an
%
\begin{align*}
  r \to \infty \quad, \quad  \begin{pmatrix}
    \alpha \sin kr + \delta_0 \\
    \beta \ee^{k'r}
  \end{pmatrix},
\end{align*}
%
wobei \[ k = \sqrt{E} \quad \text{und} \quad k' = \sqrt{ V_{\mathrm{Out}}- E}. \]
Die numerische Lösung diese Systems ist in Abbildung~\ref{fig:2014-05-28-6} zu sehen.
%
\begin{figure}[ht]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (-.1,0) -- (4.2,0) node[below] {$\mu_B B$};
    \draw[->] (0,-1) -- (0,1.7) node[left] {$a$};
    \draw[dotted] (0,.5) -- (4,.5);
    \draw[MidnightBlue] (-.5,.5) node[left] {$a_\textnormal{BG}$} -- (0,.5) to[out=0,in=-95] (2-.1,1.5);
    \draw[MidnightBlue] (2+.1,-1) to[out=85,in=180] (4,.5);
    \draw (2,.1) -- (2,-.1) node[below] {$B_0$};
  \end{tikzpicture}
  \caption{Numerische Lösung der Schrödingergleichung mit Hamiltonoperator~\eqref{eq:2014-05-28-1}.}
  \label{fig:2014-05-28-6}
\end{figure}
%
Für die Streulänge $a$ gilt
%
\begin{align}
  a = a_{\mathrm{BG}} \left(1-\frac{\Delta}{(B-B_0)}\right)
\end{align}
%
und somit führt $B = B_0$ zu einer Divergenz der Streulänge.
