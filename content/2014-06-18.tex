% Vorlesung am 28.05.2014
% TeX: Michi

\renewcommand{\printfile}{2014-06-18}

\chapter{Ultrakalte Atome}

\section{Bose Einstein Kondensation}
%
\begin{theorem}[Merkregel]
  Sei $n$ die Teilchendichte, $C$ eine reelle Konstante so gilt für die kritische Temperatur der \acct{Bose Einstein Kondensation} $T_{\mathrm{C}}$
  %
  \begin{align*}
    k_{\mathrm{B}} T_{\mathrm{C}} &= C \cdot \text{Energie}(n,\hbar,m) \\
    &= C \frac{\hbar^2}{2m} n^{2/3},
  \end{align*}
  %
  wobei die Energie von $\hbar$, $n$ und $m$ abhängt, bzw. über die Energie-Impuls Beziehung. In einer harmonischen Falle wie etwa in Abbildung~\ref{fig:2014-06-18-1} gilt
  %
  \begin{align*}
    n &= \frac{N}{R^3} \quad \text{und} \quad R \propto \sqrt{\frac{\kB T}{m \omega_0^2}} \\
    \implies \kB T_{\mathrm{C}} &= C \hbar \omega_0 N^{1/3}.
  \end{align*}
  %
  Eine explizite Rechnung zeigt, dass $C = 0.94$. Typischerweise gilt
  %
  \begin{align}
    \kB T_{\mathrm{C}} \gg \frac{\hbar \omega_0}{2}.
  \end{align}
  %
\end{theorem}
%
\begin{figure}[ht]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[name path=x,->] (0,-.3) -- (0,3);
    \draw plot[domain=-sqrt(2):sqrt(2)] (\x,{(\x)^2});
    \node[dot] at (0,0) {};
    \draw[MidnightBlue] (-.5,{(-.5)^2}) node[left] {$\frac{\hbar\omega}{2}$} -- (.5,{(.5)^2});
    \draw[<->] (0,1) -- node[above] {$R$} (1,1);
    \draw[<->] (1,1) -- node[right] {$\kB T$} (1,0);
  \end{tikzpicture}
  \caption{Harmonisches Potential mit eingezeichneter Nullpunktenergie und Energie bei der kritischen Temperatur für die Bose Einstein Kondensation.}
  \label{fig:2014-06-18-1}
\end{figure}
%

\subsection{Effekt der Atom-Atom Wechselwirkung}

Unter Verwendung eines Harten-Kugel-Potential, auch Pseudopotential genannt, lösen wir die Schrödingergleichung für zwei Teilchen in einem 3-dimensionalen Kastenpotential wie in Abbildung~\ref{fig:2014-06-18-2} zusehen ist.
%
\begin{figure}[ht]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[<->] (0,2) node[left] {$V$} |- (3,0) node[below] {$r$};
    \draw[MidnightBlue,dashed] (.5,2) -- (.5,0) node[below] {$a$};
    \draw (2.5,2) -- (2.5,0) node[below] {$b$};
    \fill[pattern=north east lines] (2.5,2) rectangle (2.7,0);
    \draw[MidnightBlue] (.5,0) to [bend left] (2.5,0);
    \node[pin={85:Ausschlussvolumen}] at (.25,1.5) {};
  \end{tikzpicture}
  \caption{Kastenpotential mit eingezeichneter Streulänge $a$ und Wellenfunktion. Der Teil links von der gestrichelten Linie wird als Ausschlussvolumen bezeichnet, dass aus dem Pseudopotential resultiert.}
  \label{fig:2014-06-18-2}
\end{figure}
%
Dazu verwenden wir den Lösungsansatz
%
\begin{align*}
  P(r) &= r R(r)
\end{align*}
%
für die Radialgleichung. Beschränken wir uns auf $s$-Wellen ($\ell = 0$) gilt
%
\begin{align}
  \left\{ - \frac{\hbar^2 }{2 m } \frac{\diff^2}{\diff r^2}  + V(r) \right\} P(r) &= E P(r).
\end{align}
%
Es gelten die Randbedingungen 
%
\begin{align*}
  P(r) = \begin{cases}
    0 & \text{für } r=a \\
    0 & \text{für } r=b
  \end{cases} \quad. 
\end{align*}
%
Damit folgt 
%
\begin{align*}
  P &= C \sin\left(k(r-a)\right) \quad \text{mit}\quad k(b-a) = \pi n\\
  E_n &= \frac{\hbar^2 k^2}{2 m_{\mu}} =\frac{\hbar^2 \pi^2 n^2}{w m_{\mu} (b-a)^2}.
\end{align*}
%
Betrachten wir den Fall $a\ll b$, d.h. wenn die Streulänge viel kleiner ist als der Kasten, so erfolgt eine Entwicklung
%
\begin{align*}
   E_{n=1} &= \frac{\hbar^2 \pi^2}{2 m_{\mu} b^2} \left(1- \frac{a}{b}\right)^{-2} \approx E(a=0) + \frac{\hbar^2 \pi^2 a}{m_{\mu} b^3} + \ldots
\end{align*}
%
Der erste Term beschreibt hierbei die Nullpunktenergie im Kasten ohne Wechselwirkung, der zweite Term die erste Korrektur, die linear in $a$ ist (eine lineare Energieverschiebung). Der Faktor $b^3$ hat hierbei die Einheit eines Volumens. Es zeigt sich, dass die Störung proportional zu $a$ ist. Die Radialwellenfunktion $R(r)$ ist dann (mit Entwicklung um kleine $r$)
%
\begin{align}
  R(r) &= \frac{P(r)}{r} = \frac{C \sin(k(r-a))}{r} \\
  &\approx \left(1 - \frac{a}{r}\right). 
\end{align}
%
D.h. die harte Kugel kostet Energie. Die Radialfunktion ist in Abbildung~\ref{fig:2014-06-18-3} zu sehen.
%
\begin{figure}[ht]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[name path=xy,<->] (0,2) node[left] {$R(r)$} |- (4.7,0) node[below] {$r$};
    \draw[MidnightBlue,dashed] (.5,2) -- (.5,0) node[below] {$a$};
    \draw[name path=f,MidnightBlue] plot[smooth,domain=1.5:4.5] (\x,{2*sin(deg(\x-.5))/\x});
    \draw[DarkOrange3] plot[smooth,domain=.5:1.5] (\x,{2*sin(deg(\x-.5))/\x});
    \draw[name intersections={of=xy and f}] (intersection-1 |- 0,.1) -- (intersection-1 |- 0,-.1) node[below] {$\lambda_{\mathrm{th}}$};
    \draw[MidnightBlue] (0,{2*sin(deg(1.5-.5))/1.5}) -- node[pin={45:$\frac{\sin(kr)}{r}$}] {} (1.5,{2*sin(deg(1.5-.5))/1.5});
    \node[pin={-90:Ausschlussvolumen}] at (.25,.25) {};
  \end{tikzpicture}
  \caption{Radialfunktion $R(r)$. Die Radialfunktion schneidet die $r$-Achse bei der Streulänge $a$ und der thermischen deBroglie Wellenlänge $\lambda_{\mathrm{th}}$.}
  \label{fig:2014-06-18-3}
\end{figure}
%

\subsection{Grundzustand eines BEC in einer harmonischen Falle}
Wir betrachten das harmonische Potential
%
\begin{align}
  V(r) &=  \frac{1}{2} m \omega^2 r^2
\end{align}
%
und berechnen das Eigenfunktional für eine Testfunktion
%
\begin{align}
  \psi &= A \cdot \ee^{-r^2/(2b^2)}, \label{eq:2014-06-18-1}
\end{align}
%
wobei $b$ die Breite des Gauß ist. Der Hamiltonoperator hat die Form
\[
  \hat{H} = \frac{p^2}{2m} + V + g \vert \psi \vert^2.
\]
Somit gilt
%
\begin{align}
  \braket{\psi|H|\psi} = E(\psi) = \int \bigg[ \frac{\hbar^2}{2m} \vert \nabla \psi \vert^2 + V(r)  \vert \psi \vert^2 + \underbrace{\frac{4 \pi \hbar^2 a}{m_{\mu}}}_{=g} \vert \psi \vert^4 \bigg] \diff r,
  \label{eq:2014-06-18-neu1}
\end{align}
%
d.h. aufgrund der Isotropie bleibt bei der Integration über das gesamte Volumen nur noch die $r$ Integration übrig.
Mit dem Ansatz~\eqref{eq:2014-06-18-1} wird die Energie nur abhängig von $b$ sein. Abbildung~\ref{fig:2014-06-18-4} zeigt die Energie in Abhängigkeit des Parameters $b$.
%
\begin{figure}[ht]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[<->] (0,2) node[left] {$E(b)$} |- (3,0) node[below] {$b$};
    \begin{scope}[shift={(1.5,1)}]
      \draw plot[domain=-1:1] (\x,{(\x)^2});
      \node[dot] at (0,0) {};
    \end{scope}
  \end{tikzpicture}
  \caption{Energie aus \eqref{eq:2014-06-18-neu1} aufgetragen über der Potentialbreite $b$.}
  \label{fig:2014-06-18-4}
\end{figure}
%
Insgesamt erhalten wir
%
\begin{align*}
  \frac{E(b)}{N} &= \frac{3}{4} \hbar \omega \left\{ \frac{a_{\mathrm{HO}}^2}{b^2} + \frac{b^2}{a_{\mathrm{HO}^2}} \right\} + \frac{g N}{(2\pi)^{3/2}} \frac{1}{b^3},
\end{align*}
%
hierbei ist $a_{\mathrm{HO}}$ die harmonische Oszillator Länge (Ausdehnung des Gaus im HO-Potential).
\begin{itemize}
  \item Für $g=0$ ist $E_{\mathrm{min}}(b) = \frac{3}{2} \hbar \omega$ mit $b_{\mathrm{min}} = a_{\mathrm{HO}} = \sqrt{\hbar /(m \omega)}$, d.h. wir erhalten den Grundzustand des  dreidimensionalen harmonischen Oszillator.
  \item Für $g>0$ (repulsive Wechselwirkung), betrachte dazu $b=a$
  %
  \begin{align*}
    \frac{E_{\mathrm{int}}}{E_{\mathrm{kin}}} &= \frac{g N }{a_{\mathrm{HO}}^3 \hbar \omega} \frac{4}{3 (2 \pi)^{3/2}} \\
    &= \underbrace{\frac{4 \cdot 4 \pi}{3(2\pi)^{3/2}}}_{\approx 1}\hbar^2 \frac{a N }{m a_{\mathrm{HO}}} \frac{m \omega}{\hbar h \omega} \\
    &= \frac{a}{a_{\mathrm{HO}}} N,
  \end{align*}
  %
  d.h. für $N \gg \frac{a_{\mathrm{HO}}}{a}$ ist $E_{\mathrm{int}} \gg E_{\mathrm{kin}}$.
\end{itemize}

Für große $N$ können wir die \acct{Thomas-Fermi Näherung} anwenden
%
\begin{gather*}
  \hat{H} = \underbrace{\frac{p^2}{2m}}_{\approx 0} + V(r) + g \vert \psi \vert^2 \\
  \implies \bigl[ V(r) + g |\psi|^2 \bigr] \ket{\psi} = \mu \ket{\psi}
  %\mu \psi  \left[ V(r) + g \vert \psi \vert^2 \right],
\end{gather*}
%
wobei $\mu$ das \acct*{chemische Potential} \index{Chemische Potential} ist. Es beschreibt die Kosten an Energie, die bezahlt werden müssen um 1 Teilchen dem System zuzuführen.  Somit folgt
%
\begin{align*}
  \mu &= \frac{m \omega^2}{2} R_{\mathrm{TF}}^2 \\
  n & \propto \vert \psi \vert^2 \propto n_0 \left(1- \frac{r^2}{R_{\mathrm{TF}}^2}\right) \quad \text{und} \quad n_0 = \frac{N_\mu}{g},
\end{align*}
%
hierbei ist $R_{\mathrm{TF}}$ der \acct{Thomas-Fermi Radius} der in Abbildung~\ref{fig:2014-06-18-5} veranschaulicht ist. Das chemische Potential ist durch die Normierung gegeben
%
\begin{figure}[ht]
  \centering
  \begin{tikzpicture}[gfx]
    \draw plot[domain=-2:2] (\x,{(\x)^2}) node[right] {$V(r)$};
    \draw[MidnightBlue] plot[domain=-1:1] (\x,{-(\x)^2+2});
    \draw[MidnightBlue] (-1,1) -- (1,1);
    \node[dot] (O) at (0,0) {};
    \node[MidnightBlue,dot] at (0,1) {};
    \draw[<->] (O) -- node[below] (RTF) {$R_{\mathrm{TF}}$} (1,0);
    \node[pin={-60:Thomas-Fermi-Radius}] at (RTF) {\phantom{$R_{\mathrm{TF}}$}};
    \draw[<->] (1,0) -- node[right] {$\mu$} (1,1);
  \end{tikzpicture}
  \caption{$V(r)$ unter der Thomas-Fermi Näherung mit eingezeichneten chemischen Potential $\mu$ und Thomas Fermi Radius $R_{\mathrm{TF}}$.}
  \label{fig:2014-06-18-5}
\end{figure}
%
\begin{align*}
  1 &= \int \vert \psi \vert^2 \diff V = \frac{\mu}{g} \frac{8 \pi }{15} R^3 ,
\end{align*}
%
somit erhalten wir für $\mu$
%
\begin{align}
  \boxed{ \mu = \frac{\hbar \omega}{2} \left(\frac{15 N a}{a_{\mathrm{HO}}}\right)^{2/5} }
\end{align}
%
Wie in Abbildung~\ref{fig:2014-06-18-6} zu sehen ist, stellt die Thomas Fermi Näherung eine unphysikalische Lösung am Rand dar, da sie hier unstetig ist. Eine echte Lösung, bspw. mittels einer numerischen Methode bestimmt ist in Abbildung~\ref{fig:2014-06-18-7} zu sehen. Die Größe $\xi$ stellt hierbei die \acct{Healing Länge} dar.
%
\begin{figure}[ht]
  \centering
        \begin{tikzpicture}[gfx]
    \draw plot[domain=-2:2] (\x,{(\x)^2});
    \draw[MidnightBlue] plot[domain=-.8:.8] (\x,{-(\x)^2+1+(.8)^2});
    \draw[MidnightBlue] (-1.5,1) -- (1.5,1);
    \node[MidnightBlue,below right] at (1,1) {$\mu$};
    \draw[DarkOrange3] (-.9,1) circle (.2) (.9,1) circle (.2);
  \end{tikzpicture}
  \caption{Veranschaulichung der Unstetigkeit am Rand von $V(r)$ unter der Thomas-Fermi Näherung.}
  \label{fig:2014-06-18-6}
\end{figure}
%
%
\begin{figure}[ht]
  \centering
  \begin{tikzpicture}[gfx]
    \draw plot[domain=-2:2] (\x,{(\x)^2});
    \draw[MidnightBlue] plot[domain=-1:1] (\x,{-(\x)^2+2});
    \draw[MidnightBlue] (-1,1) -- (1,1);
    \draw (1,1) circle (.2);
    \path (1,1)+(-20:.2) node (anchor1) {};
    \begin{scope}[scale=5,shift={(0,-.5)}]
      \draw[clip] (1,1) circle (.3);
      \draw plot[smooth,domain=-2:2] (\x,{(\x)^2});
      \draw[MidnightBlue,dashed] plot[smooth,domain=-1:1] (\x,{-(\x)^2+2});
      \draw[MidnightBlue] plot[smooth,domain=-1:1.5] (\x,{10*(\x)^2*exp(-sqrt(20)*(\x)^2)+1});
      \draw[MidnightBlue] (-1,1) -- (1.5,1);
      \path (1,1)+(-130:.3) node (anchor2) {} (1,1) coordinate (desc);
    \end{scope}
    \node[MidnightBlue,below right] at (desc) {$\ee^{-r/\xi}$};
    \draw[<->] (anchor1) to[bend right] (anchor2);
  \end{tikzpicture}
  \caption{Numerisch Lösung am Rand des Potentials $V(r)$.}
  \label{fig:2014-06-18-7}
\end{figure}
%
\minisec{Kastenpotential}

Die Thomas Fermi Näherung führt auch beim Kastenpotential auf eine unphysikalische Lösung am Rand, wie in Abbildung~\ref{fig:2014-06-18-9} zu sehen ist.
%
\begin{figure}[ht]
  \centering
  \begin{tikzpicture}[gfx]
    \draw (0,3) node[left] {$V(r)$} |- (4,0) -- (4,3);
    \draw[DarkOrange3] (0,2) -- node[pin={above:TF-Näherung}] {} (4,2);
    \draw[MidnightBlue,dashed] (0,0) to[out=0,in=180,looseness=.5] coordinate (mid) (.5,2);
    \node[MidnightBlue,below] at (0,0) {echte Lösung};
    \draw[MidnightBlue,dashed] (3.5,2) to[out=0,in=180,looseness=.5] (4,0);
    \draw[<-,MidnightBlue] (mid) -- +(.5,0);
    \draw[<-,MidnightBlue] (mid -| 0,0) -- +(-.5,0) node[above] {$\xi$};
  \end{tikzpicture}
  \caption{Kastenpotential $V(r)$ unter der Lösung der Thomas Fermi Näherung und vergleichende numerische Lösung.}
  \label{fig:2014-06-18-9}
\end{figure}
%
\begin{notice}
  Eine Quantenflüssigkeit passt sich dem Potential an.
\end{notice}