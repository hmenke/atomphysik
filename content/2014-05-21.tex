% Vorlesung am 21.05.2014
% TeX: Michi

\renewcommand{\printfile}{2014-05-21}

\section{Streuproblem}

Im Folgenden betrachten wir eine von links einfallende unendlich ausgebreitete, ebene Welle die auf ein Streuhindernis (bspw. ein Atom) $V(\bm{r})$ fällt. Resultat ist eine um den Winkel $\theta$ auslaufende Welle $\phi_S$ die in ein Raumelement $\mathrm{d}\bm{A}$ gestreut wird. Die Abstände der einfallenden Wellenfronten entsprechen der de Broglie Wellenlänge $\lambda_{\mathrm{dB}}$. Siehe dazu Abbildung~\ref{fig:2014-05-21-1}.  
%
\begin{figure}[ht]
  \centering
\begin{tikzpicture}[gfx]
    \foreach \i in {0,.5,...,1.5} {
      \draw[DarkOrange3] (\i,1) -- (\i,-1);
    }
    \draw[DarkOrange3,<->] (.5,1) -- node[above] {$\lambda_\textnormal{dB}$} (1,1);
    \draw[DarkOrange3,->] (.75,0) -- +(1.3,0) node[above] {$\bm{k}_0$};
    \draw[pattern=north east lines] (3,0) circle (.3);
    \node[below] at (3,-.3) {$V(\bm{r})$};
    \draw[dashed] (3.3,0) -- +(2,0);
    \begin{scope}[shift={(3,0)}]
      \draw[MidnightBlue] (20:.3) -- (20:3) node[below] {$\diff A$};
      \draw[MidnightBlue,->] (30:.3) -- (30:2.5);
      \draw[MidnightBlue,dashed] (30:2.5) -- (30:3);
      \draw[MidnightBlue,rotate=120] (-90:2.5) ellipse (.45 and .2);
      \draw[MidnightBlue] (40:.3) -- node[above] {$\bm{k}$} (40:3);
      \draw[MidnightBlue,->] (1.5,0) arc (0:30:1.5);
      \node[MidnightBlue] at (11:1.2) {$\vartheta$};
    \end{scope}
  \end{tikzpicture}
  \caption{Streuung einer einfallenden, ebenen Welle $\phi_0$ an einem Streupotential $V(\bm{r})$ und die resultierende auslaufende Streuwelle $\phi_S$.}
  \label{fig:2014-05-21-1}
\end{figure}
%
Gesucht wird eine stationäre Lösung der Schrödingergleichung. Aufgrund ihrer Linearität ist diese Gerade durch die Superposition von ein- und auslaufender Welle gegeben.
%
\begin{align}
  \phi  = \phi_0 + \phi_S
\end{align}
%
d.h.
%
\begin{align}
  \left[ \frac{p^2}{2 m_\mu} + V(\bm{r}) \right] \phi(\bm{r}) &= E \phi(\bm{r}),
\end{align}
%
hierbei ist $m_\mu$ die reduzierte Masse.

\begin{notice}
  Behandlung im Schwerpunktsystem, d.h. wir machen eine Separation der Schwerpunktbewegung. Dazu führen wir die reduzierte Masse  $m_\mu$  ein und betrachten den Fall gleicher Massen, also $m_1 = m_2 = m$ und erhalten
  %
  \begin{align*}
    m_\mu &= \frac{m_1 m_2 }{m_1 +m_2} = \frac{m}{2}.
  \end{align*} 
  %
  Die Energie $E$ ist abhängig von der Temperatur 
  %
  \begin{align*}
    E &= k_{\mathrm{B}} T = \frac{p^2}{2 m_\mu}.
  \end{align*}
  %
  Ab jetzt betrachten wir $\bm{p} = \hbar \bm{k}_0$ als thermischem Mittelwert des Impulses. Später muss dann über eine thermische Impulsverteilung gemittelt werden. 
\end{notice}
%
Um den richtigen Ansatz zu erhalten, verdeutlichen wir uns das Verhalten der auslaufenden Streuwelle für $r \to \infty$
%
\begin{align}
  \phi_S(r \to \infty) = f(\theta,\varphi) \frac{\ee^{\ii k r}}{r},
\end{align}
% 
wobei $f(\theta,\varphi)$ die Streuamplitude der Welle ist. 

Ziel der Streutheorie ist die Bestimmung der \acct{Wirkungsquerschnitt} $\sigma_{\mathrm{tot}}$. Der Wirkungsquerschnitt ist hierbei die Anzahl der Streuereignisse geteilt durch die Anzahl der Teilchen, genauer gesagt
%
\begin{align*}
  \sigma_{\mathrm{tot}} &\hateq \frac{\text{\# Streuereignisse}}{\cancel{Zeit} \cdot \frac{\text{\# Teilchen }}{\cancel{Zeit} \cdot \text{Fläche}}}.
\end{align*}
% 
Das Ergebnis ist somit eine Wahrscheinlichkeit für die Streuung in Einheiten einer Fläche. Für die Streuung in ein Raumwinkelelement $\mathrm{d}\Omega$ gilt
%
\begin{align}
  \diff \sigma(E,\theta,\varphi) = \lvert f(E,\theta,\varphi) \rvert^2 \diff \Omega.
\end{align}
%
\begin{notice}
  Der klassische Grenzfall des Schattenwurfs hinter einem Streuobjekt erklärt sich durch destruktive Interferenz von $\phi_0$ und $\phi_S$ in Vorwärtsrichtung.
\end{notice}
%
Für isotrope Potentiale gilt die Drehimpulserhaltung, d.h. wir können eine Partialwellenzerlegung nach den Drehimpulseigenfunktionen machen
%
\begin{align}
  \phi_{\bm{r}} &= \sum\limits_{\ell=0}^{\infty} \frac{u_\ell(r)}{r} P_\ell(\cos \theta) \label{eq:2014-05-21-1}.
\end{align}
% 
Hierbei sind $P_\ell(\cos\theta)$ die Legendre-Polynome und es wurde eine magnetische Quantenzahl $m=0$ vorausgesetzt. Setzten wir Gleichung~\eqref{eq:2014-05-21-1} in die Schrödingergleichung ein, erhalten wir
%
\begin{align}
  \frac{\hbar^2}{2 m_\mu} u_\ell''(r) + \left[E - V_{\mathrm{eff}}\right] u_\ell(r) = 0,
\end{align}
%
wobei
%
\begin{align}
  E_{\mathrm{eff}} = V(\bm{r}) + \frac{\hbar^2 \ell (\ell +1)}{2 m_\mu r^2}
\end{align}
%
ein effektives Potential mit Potentialbarriere ist. 

\minisec{Lösung}

\begin{enumerate}
  \item Für ein freies Teilchen, also $V(r)=0$ addieren sich die Partialwellen zu einer ebenen Welle.
  \item Für $V(r) \neq 0$ aber $V(r) \to 0$ für $r \to \infty$ (Kurzreichweitig) erhalten wir die Lösung wie im freien Fall bis auf eine Phasenverschiebung \[\delta_\ell (k)\] die durch den \frqq inneren\flqq\ Teil $V(r) \neq 0$ erzeugt wird. Für die Streuamplitude dargestellt in den Drehimpulseigenfunktionen ergibt sich
  %
  \begin{align}
    f(k,\theta) = \frac{1}{k} \sum\limits_{\ell} (2 \ell +1) \ee^{\ii \delta_\ell } \sin \delta_\ell P_\ell (\cos \theta).
  \end{align}
  %
  Der \acct{Streuquerschnitt} ist somit
  %
  \begin{align}
    \boxed{ \sigma_{\mathrm{tot}}= \frac{4 \pi }{k^2} \sum\limits_{\ell} (2 \ell +1 ) \sin^2\delta_\ell  }
  \end{align}
  %
  Hierbei gilt 
  %
  \begin{align*}
    \sigma_{\ell}(k) = (2 \ell +1) \sin^2\delta_\ell.
  \end{align*}
  % 
\end{enumerate}

\begin{notice}
  \begin{enumerate}
    \item Der maximale Wirkungsquerschnitt (unitarity limit) für $\sin^2\delta_\ell = 1$ ist
    %
    \begin{align*}
      \sigma_\ell = \frac{4 \pi}{k^2} (2 \ell +1) \approx \lambda_{\mathrm{th}}^2.
    \end{align*}
    %
    \item Nützliche Beziehung für $s$-Wellen ($\ell =0$) sind
    %
    \begin{align*}
      f_0(k) &= \frac{1}{k} \ee^{\ii \delta_0(k)} \sin\delta_\ell \\
      \Im \frac{1}{f_0} &= - k \\
      \Re \frac{1}{f_0} &= k \cot \delta_0(k) \\
      \implies f_0(k) &= \frac{1}{\Re\left(\frac{1}{f_0}\right) + \ii \Im\left(\frac{1}{f_0}\right)} \\
      &= \frac{1}{k \cot \delta_0 + \ii k} 
    \end{align*}
    %
    \item Für $T \to 0$, d.h. $k \to 0$ trägt nur die $s$-Welle zur Streuung bei. Das Problem reduziert sich auf die Berechnung einer einzigen Phase $\delta_0$ oder $f_0$
    %
    \begin{align*}
      f_0 &= \lim\limits_{k \to 0} \frac{1}{k} \ee^{\ii \delta(k)} \sin \delta_0(k)\\
      &\coloneq a,
    \end{align*}
    %
    wobei $a$ die \acct{Streulänge} ist und somit 
    %
    \begin{align*}
      \sigma_0 &= 4 \pi a^2
    \end{align*}
    %
    \item Für identische Teilchen gilt, dass die Gesamtwellenfunktion (anti-)symmetrisch unter Punktspiegelung werden muss (Pauli Prinzip) d.h.
    \[
      \bm{r} \to -\bm{r} \quad,\quad \theta \to \theta-\pi  \quad,\quad \varphi\to \varphi +\pi. 
    \]
    Die Streuwellenfunktion ist somit
    %
    \begin{align*}
      \phi_S(\bm{r}) \stackrel{r \to \infty}{\to} \left[ f(\theta) \pm f(\pi - \theta) \right] \frac{\ee^{\ii kr}}{r},
    \end{align*}
    %
    wobei $+$ für Bosonen und $-$ für Fermionen einzusetzen ist, dies ist in Abbildung~\ref{fig:2014-05-21-2} dargestellt.
    %
    \begin{figure}[ht]
      \centering
      \begin{tikzpicture}[gfx]
        \begin{scope}
          \draw[arrow inside={pos=0.3},rounded corners=0.5cm] (0,0) node[dot,label={left:$A$}] {} -- (2,0) -- (4,1);
          \draw[arrow inside={pos=0.3},rounded corners=0.5cm] (4,0) node[dot,label={right:$B$}] {} -- (2,0) -- (0,-1);
          \draw[dotted] (3,0) -- (2,0) -- (3,0.5);
          \draw (3,0) arc (0:30:{1*cos(30)});
          \node[right] at ($(2,0)+(15:1)$) {$\theta$};
          \node[below] at (2,-1) {Bosonen};
        \end{scope}
        \begin{scope}[xshift=6cm]
          \draw[arrow inside={pos=0.3},rounded corners=0.5cm] (0,0) node[dot,label={left:$A$}] {} -- (2,0) -- (0,-1);
          \draw[arrow inside={pos=0.3},rounded corners=0.5cm] (4,0) node[dot,label={right:$B$}] {} -- (2,0) -- (4,1);
          \draw (1,0) arc (180:210:{1*cos(30)});
          \node[left] at ($(2,0)+(195:1)$) {$\pi-\theta$};
          \node[below] at (2,-1) {Fermionen};
        \end{scope}
      \end{tikzpicture}
      \caption{Interpretation der Streuamplituden}
      \label{fig:2014-05-21-2}
    \end{figure}
    %
    Es zeigt sich, dass für identische Bosonen nur gerade Partialwellen besetzt ($s$,$d$,\ldots) und für Fermionen nur die ungeraden ($p$, $f$,\ldots) sind. im weiteren Sinne bedeutet dies, dass für $k\to0$ ein ein-komponentiges Gas aus Fermionen ideal ist!
  \end{enumerate}
\end{notice}

\begin{notice}
  \begin{itemize}
    \item Die relevante Skala ist die van-der Waals Länge $\approx \SI{5}{\milli\meter}$.
  \item Eine einzige Phase reicht aus um den Streuprozess zu charakterisieren, diese muss jedoch experimentell gefunden werden!
  \end{itemize}
\end{notice}
