% Vorlesung am 20.11.2013
% TeX: Michi

\renewcommand{\printfile}{2013-11-12}

\section{Übergänge und Auswahlregeln}
Wir betrachten induzierte Übergänge zwischen stationären Zuständen. Die dominierende Wechselwirkung ist die \acct{Dipolwechselwirkung}, für diese gilt:
\[
  \hat{H}_{WW} = -\hat{\bm{d}} \cdot \bm{E}, 
\]
wobei
\[
  \bm{E} = \left|E_0\right| \Re\left(\ee^{-\ii\omega t} \bm{\mathcal{E}}\right).
\]
Polarisationsvektor:
%
\begin{align*}
  \bm{\varepsilon}_{\pi} &=\bm{\varepsilon}_z \\
  \bm{\varepsilon}_{\pm} &= \frac{\bm{\varepsilon}_x \pm \ii \bm{\varepsilon}_y}{\sqrt{2}}.
\end{align*}
%
Für die Übergangsrate gilt:
%
\begin{align*}
  \text{Übergangsrate} \propto \left|E_0\right|^2 \bigg| \int \psi_f^{\ast}  \left(\hat{\bm{d}} \cdot \bm{\mathcal{E}}\right) \psi_i \diff V\bigg|^2
\end{align*}
%
wobei $\hat{\bm{d}} = e \cdot \hat{\bm{r}}$. Somit
%
\begin{align*}
  \text{Übergangsrate} \propto \left|e E_0\right|^2 \left|\Braket{f|\hat{\bm{r}} \cdot \bm{\mathcal{E}}|i}\right|^2.
\end{align*}
%
Der Term $\left|\Braket{f|\hat{\bm{r}} \cdot \bm{\varepsilon}|i}\right|^2$ entspricht dem \acct{Dipolmatrixelement}. Diese kann durch explizites Ausrechnen bestimmt werden. 
%
\begin{align*}
  \Braket{f|\hat{\bm{r}} \cdot \bm{\mathcal{E}}|i} &= \int\limits_{0}^{\infty} R_{n_f,\ell_f}(r) r^3 R_{n_i,\ell_i}(r) \diff r \int\limits_{0}^{2\pi}\int\limits_{0}^{\pi} Y_{\ell_f,m_f}^\ast \frac{\bm{r} \cdot \bm{\mathcal{E}}}{|\bm{r}|} Y_{\ell_i,m_i} \sin\theta \diff \theta \diff \phi
\end{align*}
%
Im Allgemeinen ist der Radialteil ungleich Null, verschwindet also nicht. Die \acct*{Auswahlregeln} \index{Auswahlregeln !Dipolübergänge} kommen aus dem Winkelanteil. Betrachte dazu zunächst
%
\begin{align}
  \frac{\bm{r}}{|\bm{r}|} &= \frac{1}{r} \left(x \bm{\varepsilon}_x + y \bm{\varepsilon}_y + z \bm{\varepsilon}_z\right) \nonumber\\
  &= \sin\theta \cos\phi \bm{\varepsilon}_x + \sin\theta \sin\phi \bm{\varepsilon}_y + \cos\theta \bm{\varepsilon}_z \nonumber\\
  &\propto Y_{1,-1} \underbrace{\frac{\bm{\varepsilon}_x + \ii \bm{\varepsilon}_y}{\sqrt{3}}}_{\bm{\varepsilon}_{-}^{\ast}} + Y_{1,0} \bm{\varepsilon}_z + Y_{1,1} \underbrace{\frac{-\left(\bm{\varepsilon}_x + \ii \bm{\varepsilon}_y\right)}{\sqrt{2}}}_{\bm{\varepsilon}_{+}^\ast} \label{eq: 2013-11-20-1}
\end{align}
%
$\bm{\mathcal{E}}$ kann wie folgt umgeschrieben werden
%
\begin{align}
  \bm{\mathcal{E}} &= A_{\sigma_{-}} \bm{\varepsilon}_{-} + A_{\pi} \bm{\varepsilon}_{z} + A_{\sigma_{+}} \left(-\bm{\varepsilon}_{+}\right) \label{eq: 2013-11-20-2}
\end{align}
%
\begin{theorem*}[Merke]
  \begin{align}
    \boxed{\bm{\varepsilon}_{+} \bm{\varepsilon}_{+}^\ast = 1}
  \end{align}
\end{theorem*}
%
Somit folgt für den Term $\frac{\bm{r} \cdot \bm{\mathcal{E}}}{|\bm{r}|}$ (bzw. Gleichung~\eqref{eq: 2013-11-20-1} mal \eqref{eq: 2013-11-20-2}):
%
\begin{align*}
  \frac{\bm{r} \cdot \bm{\mathcal{E}}}{|\bm{r}|} &\propto A_{\sigma_{-}} Y_{1,-1} + A_{\pi} Y_{1,0} + A_{\sigma_{+}} Y_{1,-1},
\end{align*}
%
wobei
\[
  Y_{1,0} = \sqrt{\frac{3}{4 \pi}} \cos\theta
\]
ist.

\begin{example}
  %
  Wir betrachten die $\pi$-Übergänge.
  \begin{align*}
    I^{\pi} &\coloneq \int Y_i^\ast \cos\theta Y_f \sin\theta \diff \theta \diff \phi
  \end{align*}
  %
  Unter Verwendung der Rotationssymmetrie um $s$, bzw. Rotation um den Winkel $\phi_0$ ergibt sich:
  %
  \begin{align*}
    I^{\pi} &= \ee^{\ii\left(m_i - m_f\right)\phi_0} I^{\pi}
  \end{align*}
  % 
  Diese Gleichung ist somit nur dann erfüllt, bzw. verschwindet, wenn $m_f = m_i$ oder
  %
  \begin{align}
    \boxed{\Delta m = 0}
  \end{align}
  %
  Für die $\sigma_{\pm}$-Übergänge folgt analog 
  \begin{align}
    \boxed{\Delta m = \pm 1},
  \end{align}
  wobei $\Delta m = m_f - m_i$ zu beachten ist.
\end{example}

\minisec{Weitere Auswahlregeln}
Weitere Auswahlregeln folgen aus dem Winkelanteil, dazu betrachte
%
\begin{align}
  \int Y_f^{\ast} Y_{1m} Y_i \diff \Omega \label{eq: 2013-11-20-3}
\end{align}
%
Durch Verwendung der Eigenschaften von Kugelflächenfunktionen:
%
\begin{align*}
  Y_{1m} Y_{\ell m} &= A Y_{\ell_1+1,m+m_1} + B Y_{\ell_1-1, m_1-m}
\end{align*}
%
wobei $A$ und $B$ sind bekannte, jedoch uninteressante Konstanten. Weiter gilt die \acct*{Orthonormalitätsrelation} \index{Orthonormalitätsrelation ! Kugelflächenfunktionen} der Kugelflächenfunktionen: 
%
\begin{align}
  \int Y_{\ell'm'}^{\ast} Y_{\ell,m} \diff \Omega = \delta_{\ell\ell'} \delta_{mm'}.
\end{align}
%
Für Gleichung~\eqref{eq: 2013-11-20-3} bedeutet dies
%
\begin{align*}
  \int Y_f^{\ast} Y_{1m} Y_i \diff \Omega &= A \delta_{\ell_f,\ell_i+1} \delta_{m_f,m_i+m} + B\delta_{\ell_f,\ell_i-1} \delta_{m_f,m_i-m}
\end{align*}
%
Diese Gleichung ist erfüllt für
%
\begin{align}
  \boxed{\Delta \ell = \pm 1}
\end{align}
%
In obiger Gleichung spiegelt sich die Drehimpulserhaltung im Lichtfeld wieder.

\begin{notice*} 
  Die hergeleiteten Dipolauswahlrelgeln geben nicht nur Auskunft darüber, ob ein Übergang erlaubt oder verboten ist, sondern auch welche Art von Polarisierung das Licht aufweist. So liegt beim $\pi$-Übergang linear polarisiertes Licht vor, bei einem $\sigma_{\pm}$-Übergang rechts oder links zirkularpolarisiertes Licht. 
\end{notice*}
